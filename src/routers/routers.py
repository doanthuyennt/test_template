"""This is routers

"""

# python standard library imports
import os
import time
import base64
import logging
import traceback

# third-party module or package imports
import cv2
import pydantic
import numpy as np

from fastapi import APIRouter
from pydantic import BaseModel
from typing import Optional, List
from fastapi import FastAPI, File, UploadFile, Form
from fastapi.encoders import jsonable_encoder
from configparser import ConfigParser

# code repository sub-package imports
from src import rcode

# init general envs

# load config
config = ConfigParser()
config.read("config/api.cfg")

# init app
router = APIRouter()

# get logger
logger = logging.getLogger(__name__)

# define class
class Images(BaseModel):
    data: Optional[List[str]] = pydantic.Field(default=None, example=None, description="List of base64 encoded images")


class PredictData(BaseModel):
    #    images: Images
    images: Optional[List[str]] = pydantic.Field(
        default=None, example=None, description="List of base64 encoded images"
    )


# TODO load and warm-up model here


# define routes and functions
@router.post("/predict")
async def predict(data: PredictData):
    # init variables
    logger.info("predict")
    return_result = {"code": "1001", "status": rcode.code_1001}

    try:
        start_time = time.time()
        predicts = []
        try:
            images = jsonable_encoder(data.images)
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = {"code": "609", "status": rcode.code_609}
            return

        for image in images:
            image_decoded = base64.b64decode(image)
            jpg_as_np = np.frombuffer(image_decoded, dtype=np.uint8)
            process_image = cv2.imdecode(jpg_as_np, flags=1)
            # uncomment for convert opencv img to pillow img
        #            process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
        #            process_image = Image.fromarray(process_image)

        # TODO processing here

        return_result = {
            "code": "1000",
            "status": rcode.code_1000,
            "predicts": predicts,
            "process_time": time.time() - start_time,
            "return": "base64 encoded file",
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = {"code": "1001", "status": rcode.code_1001}
    finally:
        return return_result


@router.post("/predict_binary")
async def predict_binary(binary_file: UploadFile = File(...)):
    # init variables
    logger.info("predict_binary")
    return_result = {"code": "1001", "status": rcode.code_1001}

    try:
        start_time = time.time()
        predicts = []
        try:
            bytes_file = await binary_file.read()
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = {"code": "609", "status": rcode.code_609}
            return

        nparr = np.fromstring(bytes_file, np.uint8)
        process_image = cv2.imdecode(nparr, flags=1)
        # uncomment for convert opencv img to pillow img
        #        process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
        #        process_image = Image.fromarray(process_image)

        # TODO processing here

        return_result = {
            "code": "1000",
            "status": rcode.code_1000,
            "predicts": predicts,
            "process_time": time.time() - start_time,
            "return": "base64 encoded file",
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = {"code": "1001", "status": rcode.code_1001}
    finally:
        return return_result


@router.post("/predict_multipart")
async def predict_multipart(argument: Optional[float] = Form(...), binary_file: UploadFile = File(...)):
    # init variables
    logger.info("predict_multipart")
    return_result = {"code": "1001", "status": rcode.code_1001}

    try:
        start_time = time.time()
        predicts = []
        try:
            bytes_file = await binary_file.read()
        except Exception as e:
            logger.error(e, exc_info=True)
            return_result = {"code": "609", "status": rcode.code_609}
            return

        nparr = np.fromstring(bytes_file, np.uint8)
        process_image = cv2.imdecode(nparr, flags=1)
        # uncomment for convert opencv img to pillow img
        #        process_image = cv2.cvtColor(process_image, cv2.COLOR_BGR2RGB)
        #        process_image = Image.fromarray(process_image)

        # TODO processing here

        return_result = {
            "code": "1000",
            "status": rcode.code_1000,
            "predicts": predicts,
            "process_time": time.time() - start_time,
            "return": "base64 encoded file",
        }
    except Exception as e:
        logger.error(e, exc_info=True)
        return_result = {"code": "1001", "status": rcode.code_1001}
    finally:
        return return_result
