import json

with open("../config/api.json") as jfile:
    config = json.load(jfile)

SERVICE_IP = config["SERVICE_IP"]
SERVICE_PORT = config["SERVICE_PORT"]
LOG_PATH = config["LOG_PATH"]