"""This is template for making a api

"""

# python standard library imports
import os
import logging
import time
import datetime
import json

# third-party module or package imports
import uvicorn
import asyncio

from fastapi import FastAPI, File, UploadFile, Form

# code repository sub-package imports
from src import rlogger
from src import rcode
from src.routers import routers

# init general envs
now = datetime.datetime.now()

# load config
with open("config/api.json") as jfile:
    config = json.load(jfile)

SERVICE_IP = config["SERVICE_IP"]
SERVICE_PORT = config["SERVICE_PORT"]
LOG_PATH = config["LOG_PATH"]

# init app
app = FastAPI()
app.include_router(routers.router)

# create logger
log_formatter = logging.Formatter("%(asctime)s %(levelname)s" " %(funcName)s(%(lineno)d) %(message)s")
log_handler = rlogger.BiggerRotatingFileHandler(
    "ali",
    LOG_PATH,
    mode="a",
    maxBytes=2 * 1024 * 1024,
    backupCount=200,
    encoding=None,
    delay=0,
)
log_handler.setFormatter(log_formatter)
log_handler.setLevel(logging.INFO)

logger = logging.getLogger("")
logger.setLevel(logging.INFO)
logger.addHandler(log_handler)

logger.info("INIT LOGGER SUCCESSED")


# print app info
print("SERVICE_IP", SERVICE_IP)
print("SERVICE_PORT", SERVICE_PORT)
print("LOG_PATH", LOG_PATH)
print("API READY")


# run app
if __name__ == "__main__":
    uvicorn.run(app, port=SERVICE_PORT, host=SERVICE_IP)
