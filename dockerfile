FROM ubuntu:18.04
WORKDIR /base

RUN ln -sf /usr/share/zoneinfo/Asia/Ho_Chi_Minh /etc/localtime

RUN apt-get update && apt-get install -y python3.7 python3-pip cmake wget llvm
RUN apt install -y python3.7-distutils curl && \
    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py &&     python3.7 get-pip.py --user
RUN apt-get install -y libglib2.0-0 libsm6 libxext6 libxrender-dev libgl1-mesa-glx
RUN python3.7 -m pip install uvicorn fastapi pymysql pandas scikit-learn
RUN python3.7 -m pip install python-multipart multipledispatch opencv-python==4.2.0.32
CMD sh start_service.sh
